import time
start_time = time.time()

#This function transforms the file into a list of list to store the hands
def get_hands():
    with open('p054_poker.txt') as f:
        samples = f.read().splitlines()
    intermediate_hands = []
    for sample in samples:
        intermediate_hands.append(sample.split())

    hands = []
    for hand in intermediate_hands:
        one_card = []
        for cards in hand:
            one_card.append([cards[:1], cards[1:]])
        hands.append(one_card)
    return hands

#This function needs to take a list of card as input and give back an integer of the highest card
#It needs to work for any lenght of
def get_highest_card(list_of_cards): 
    highest  = 0
    dict_card = {'2':2,'3':3,'4':4,'5':5,'6':6,'7':7,'8':8,'9':9,'T':10,'J':11,'Q':12,'K':13,'A':14}
    for card in list_of_cards:
        if dict_card[card] > highest:
            highest = dict_card[card]
    return highest

#This function should return a description of the value of the hand given to it
def get_value_hand(hand_of_card):
    value_hand = {'Value' : 0,'Number':0,'Highest': 0}
    if hand_of_card[0][1] == hand_of_card[1][1] == hand_of_card[2][1] == hand_of_card[3][1] == hand_of_card[4][1]:
        value_hand['Value'] = 6 #I consider that the flush is worth 6 because it's the 6th best hand
    dict_card = {'2':2,'3':3,'4':4,'5':5,'6':6,'7':7,'8':8,'9':9,'T':10,'J':11,'Q':12,'K':13,'A':14}
    ordered_cards = sorted([dict_card[hand_of_card[0][0]],dict_card[hand_of_card[1][0]],dict_card[hand_of_card[2][0]],dict_card[hand_of_card[3][0]],dict_card[hand_of_card[4][0]]])
    exists_pair, double_pair, exists_three, exists_four = False, False, False, False
    card_pair, card_three, card_four = 0, 0, 0
    for card in ordered_cards:
        if ordered_cards.count(card) == 2: #Chech if there is a pair
            if card_pair != 0 and card_pair != card: #Check if there is a double pair
                double_pair = True
            card_pair = card
            exists_pair = True
        elif ordered_cards.count(card) == 3: #Check if there is a 3 of a kind
            card_three = card
            exists_three = True
        elif ordered_cards.count(card) == 4: #Check if there is a four of a kind
            card_four = card
            exists_four = True
    if exists_four:
        value_hand['Value'] = 8
        value_hand['Number'] = card_four
    elif exists_three and exists_pair:
        value_hand['Value'] = 7
        value_hand['Number'] = card_three
    elif exists_three:
        value_hand['Value'] = 4
        value_hand['Number'] = card_three
    elif double_pair:
        value_hand['Value'] = 3
        value_hand['Number'] = card_pair
    elif exists_pair:
        value_hand['Value'] = 2
        value_hand['Number'] = card_pair

    if value_hand['Value'] == 0:
        value_hand['Value'] = 1
        value_hand['Number'] = ordered_cards[4]
    
    return value_hand

#This function should return the number of games that each player won
def get_result_games():
    results = {'Player 1' : 0, 'Player 2' : 0}
    hands = get_hands()
    for hand in hands:
        player_1_hand = hand[:5]
        player_2_hand = hand[5:]
        value_hand_player_1 = get_value_hand(player_1_hand)
        value_hand_player_2 = get_value_hand(player_2_hand)
        if value_hand_player_1['Value'] > value_hand_player_2['Value']:
            results['Player 1'] += 1
            print(str(player_1_hand) + ' - vs - ' + str(player_2_hand))
            print(str(value_hand_player_1) + ' - value - ' + str(value_hand_player_2))
        elif value_hand_player_2['Value'] > value_hand_player_1['Value']:
            results['Player 2'] += 1
        else:
            if value_hand_player_1['Number'] > value_hand_player_2['Number']:
                results['Player 1'] += 1
                print(str(player_1_hand) + ' - vs - ' + str(player_2_hand))
                print(str(value_hand_player_1) + ' - value - ' + str(value_hand_player_2))
            elif value_hand_player_2['Number'] > value_hand_player_1['Number']:
                results['Player 2'] += 1

    return results

print(get_result_games())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )